
/**
 *    轮播图
 *    <ul id="slider1">
 *        <li><a href="#"><img src=""/></a></li>
 *        <li><a href="#"><img src=""/></a></li>
 *        <li><a href="#"><img src=""/></a></li>
 *    </ul>
 *    <ol>
 *        <li class="current"></li><li></li><li></li><li></li><li></li><li></li>
 *    </ol>
 *
 *    <i class="left" style="display:none;">&#xe617;</i>
 *    <i class="right" style="display:none;">&#xe604;</i>
 *
 * 
 */

(function($){
  $.fn.slider= function(){
    return this.each(function(){
    	var $this = $(this);
        var $ol = $(this).siblings('ol');
        var img_len = $(this).children('li').length;
        var step_width = $(this).children('li:eq(0)').outerWidth(true);

        var $left_btn = $(this).siblings('.left');
        var $right_btn = $(this).siblings('.right');
        var index = 0;
        var moveId;
        /*
         * 给li添加编号
         */
        $this.children('li').each(function(index, el) {
           $(el).attr('num',index);            
        });


        function createInterval(){
                 
                      moveId = setInterval(function(){
                        if(!$this.is(':animated')){//判断当前ul是否处于动画中
                             index++;
                             if(index==img_len){
                                index = 0;
                             }
                             //console.log('========================='+step_width);
                             $ol.children('li:eq('+index+')').addClass('current').siblings('li').removeClass('current');
                             
                             $this.animate(
                                           {
                                              marginLeft: '-='+step_width+'px'
                                           },1000,function(){
                                            if(Math.abs(parseInt($this.css("margin-left"))) >= step_width){
                                                 $this.find("li").slice(0, 1).appendTo($this);
                                                 $this.css("margin-left", 0);
                                            }
                                                                                   
                                        });

                            } 
                     }, 2000);

                 
        };

        createInterval();



        //鼠标悬停
        $this.parent().hover(function() {
            /*$this.stop();*/
            clearInterval(moveId);
            $left_btn.show();
            $right_btn.show();
            
        }, function() {
            $left_btn.hide();
            $right_btn.hide();
            createInterval();           
        });





         //点击左右方向
        $left_btn.click(function(event) {
            
            if(!$this.is(':animated')){
                     --index;
                     if(index < 0){
                      index=img_len-1;
                     }
                     //console.log('========'+index);
                     
                     $ol.children('li:eq('+index+')').addClass('current').siblings('li').removeClass('current');
                     //获取最后一个li
                     var last_li = $this.find("li").slice(img_len-1,img_len);
                     //追加到第一个li后前面
                     $this.find("li:eq(0)").before(last_li);
                     //同时设置margin-left= -step_width
                     $this.css('margin-left', '-'+step_width+'px');
                     $this.animate(
                                   {
                                      marginLeft:'0px'
                                   },500,function(){ 
                                });
             }
            
        });

        $right_btn.click(function(event) {

            if(!$this.is(':animated')){
                ++index;
                if(index>=img_len){
                  index = 0;
                }
                $ol.children('li:eq('+index+')').addClass('current').siblings('li').removeClass('current');
                $this.animate(
                               {
                                  marginLeft: -step_width+'px'
                               },500,function(){  
                                 $this.find("li").slice(0, 1).appendTo($this);
                                 $this.css("margin-left", 0);                                                                
                            }); 
             }

        });



         //点击小圆点
        $ol.children('li').click(function(){
                if(!$this.is(':animated')){
                    var _index  = $(this).index(); 
                    //获取 ol 对应的li在ul中当前的索引值
                    var crrent_index = $this.children('li[num='+_index+']').index();

                    //console.log('--------------'+crrent_index);

                    //截取 ol 对应的当前的li及后面所有的li
                    var last_li_arr = $this.find("li").slice(crrent_index, img_len);
                    //放在当前正在显示li的前面
                    $this.find("li:eq(0)").before(last_li_arr);
                    //设置 ul的 margin-left值
                    $this.css('margin-left', '-'+(step_width*last_li_arr.length)+'px');

                    $ol.children('li:eq('+_index+')').addClass('current').siblings('li').removeClass('current');
                    $this.animate(
                               {
                                  marginLeft: '0px'
                               },500,function(){
                                index  = _index;                                                                 
                            });

                }
                

        });


    });

  };

})(window.jQuery);